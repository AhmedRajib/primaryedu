//
//  aboutVC.swift
//  projectAid
//
//  Created by Flowdigital Media01 on 12/15/19.
//  Copyright © 2019 Flowdigital Media01. All rights reserved.
//

import UIKit

class aboutVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "startVC") as? StartVC
        {
            navigationController?.pushViewController(vc, animated: true)
            
            present(vc, animated: true, completion: nil)
        }

    }
    
}
