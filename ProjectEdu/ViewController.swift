//
//  ViewController.swift
//  projectAid
//
//  Created by Flowdigital Media01 on 12/4/19.
//  Copyright © 2019 Flowdigital Media01. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let color = ["#C1F0F6","B0E0E6","#98F5FF","#8EE5EE","#7AC5CD","#05B8CC","#73B1B7","#00868B","#53868B","#2F4F4F"]
    

    var list = ["Flowers","Birds","Fish","Fruits","Animals","Sports","Vehicle","Flags","Bangladesh"]
    
    
    var imgTxt = ["flower","bird","fish","fruits","animal","sport","vehicle","flags","bangladesh"]
    
   
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // change the navigationBar colore
        navigationBar()
        
    }


}
extension ViewController: UITableViewDataSource,UITableViewDelegate {
    
  

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.lbl.text = list[indexPath.row]
        
      //  cell.img.layer.borderWidth

   
       cell.img.image = UIImage(named: imgTxt[indexPath.row])

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = colorForIndex(index: indexPath.row)
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
         let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "test")
        
            //            navigationController?.pushViewController(vc, animated: true)
            
            //(vc, animated: true, completion: nil)
            
            present(vc, animated: true, completion: nil)
        
        
    }
    

}

// this Extension for all function

extension ViewController{
    
    func checkForLogin() {
        
    }
    
    func colorForIndex(index: Int) -> UIColor {
        let itemCount = list.count - 1
        
        print(" indez \(itemCount)")
        let color = (CGFloat(index) / CGFloat(itemCount)) * 0.5
        
        print("Color \(color)")
        return UIColor(red: 0.0, green: color, blue: 0.1, alpha: 0.5)
    }
    
    func navigationBar () {
        navigationController?.navigationBar.barTintColor = UIColor.brown
    }
    
}

