//
//  StartVC.swift
//  projectAid
//
//  Created by Flowdigital Media01 on 12/8/19.
//  Copyright © 2019 Flowdigital Media01. All rights reserved.
//

import UIKit

class StartVC: UIViewController {
    
    let color = UIColor.red
    
    @IBOutlet weak var playButton: UIButton!
    // All Outlets of StartVC
    @IBOutlet weak var setting: UIButton!
    @IBOutlet weak var Score: UIButton!
    @IBOutlet weak var about: UIButton!
    
    
   
    override func viewDidLoad() {
        
        
   
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setting.backgroundColor = UIColor.clear
       
        
        //score board setting
        
        Score.backgroundColor = UIColor.clear
        
        about.backgroundColor = UIColor.clear
        
    }
    
    
    // all Button Action

    @IBAction func Score(_ sender: UIButton) {
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "scoreVC") as? scoreVC
        {
           // navigationController?.pushViewController(vc, animated: true)
            present(vc, animated: true, completion: nil)
            
        }

    }
    
    @IBAction func setting(_ sender: UIButton) {
        
        
        print("Clicked The Setting button ")
        
//        self.performSegue(withIdentifier: "seeting", sender: nil)
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "seetting") as? settingVC
        {
//            navigationController?.pushViewController(vc, animated: true)
            
            //(vc, animated: true, completion: nil)
            
            present(vc, animated: true, completion: nil)
        }

        
    }

    
    @IBAction func about(_ sender: UIButton) {
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "aboutVC") as? aboutVC
        {
            navigationController?.pushViewController(vc, animated: true)
            
            present(vc, animated: true, completion: nil)
        }
    }
    
 
    
}
    
    


