//
//  scoreVC.swift
//  projectAid
//
//  Created by Flowdigital Media01 on 12/15/19.
//  Copyright © 2019 Flowdigital Media01. All rights reserved.
//

import UIKit

class scoreVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "startVC") as? StartVC
        {
            navigationController?.pushViewController(vc, animated: true)
            
            present(vc, animated: true, completion: nil)
        }

    }
    

}
